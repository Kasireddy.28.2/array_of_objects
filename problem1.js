 function emailAddressOfIndividuals(array){
    if(Array.isArray(array)){
        let email_address={};
        for(let data of array){
            email_address[data.name]=data.email;
        };

        return email_address;
    }else{
        return [];
    }
 };

 module.exports=emailAddressOfIndividuals;