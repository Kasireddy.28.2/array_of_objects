
function nameAndCityOfIndividual(array,position){
    if(Array.isArray(array)){
        let name_and_city_of_individual={};
        for(let index=0;index<array.length;index++){
            if(index===position){
                name_and_city_of_individual["name"]=array[index].name;
                name_and_city_of_individual["city"]=array[index].city;

            };
        };
        return name_and_city_of_individual;
    };
};

module.exports=nameAndCityOfIndividual;