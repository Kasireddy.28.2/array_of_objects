function hobbiesOfIndividuals(array,age){
    if(Array.isArray(array)){
        let hobbies={};
        for(let data of array){
            if(data.age===age){
            hobbies[data.name]=data.hobbies;
        }
        };
        return hobbies;
            
    }else{
        return [];
    }
};

module.exports=hobbiesOfIndividuals;