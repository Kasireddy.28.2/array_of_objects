function cityAndCountryOfEachIndividual(array){
    if(Array.isArray(array)){
        let city_and_country_of_individuals={};
        for(let data of array){
            city_and_country_of_individuals[data.name]={};
            city_and_country_of_individuals[data.name]["city"]=data.city;
            city_and_country_of_individuals[data.name]["country"]=data.country;
        };

        return city_and_country_of_individuals;
    };

};

module.exports=cityAndCountryOfEachIndividual;