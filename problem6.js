function firstHobbyOfIndividual(array){
    if(Array.isArray(array)){
        let hobby_of_individual={};
        for(let data of array){
            hobby_of_individual[data.name]=data.hobbies[0];

        };
        return hobby_of_individual;
    };

};

module.exports=firstHobbyOfIndividual;