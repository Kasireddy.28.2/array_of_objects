
function loopToAccessageOfIndividuals(array){
    if(Array.isArray(array)){
        let ages_of_individuals={};
        for(let data of array){
            ages_of_individuals[data.name]=data.age;

        };

        return ages_of_individuals;
    };
};

module.exports=loopToAccessageOfIndividuals;