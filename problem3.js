
function individualsLivesInAustraliaAndIsStudent(array,country){
    if(Array.isArray(array)){
        let individuals_in_australia_and_is_student=[];
        for(let data of array){
            if(data.isStudent && data.country===country){
                individuals_in_australia_and_is_student.push(data.name);
            };

        };
        return individuals_in_australia_and_is_student;
    };
};

module.exports=individualsLivesInAustraliaAndIsStudent;