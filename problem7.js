function namesAndEmailsOfIndividualsAged25(array,age){
    if(Array.isArray(array)){
        let names_and_emails_of_individuals_aged_25={};
        for(let data of array){
            if(data.age===age){
                names_and_emails_of_individuals_aged_25["name"]=data.name;
                names_and_emails_of_individuals_aged_25["email"]=data.email;
            };
        };
        return names_and_emails_of_individuals_aged_25;
    };
};


module.exports=namesAndEmailsOfIndividualsAged25;